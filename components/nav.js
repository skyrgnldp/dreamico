import React, {Component} from 'react'
import {
  Container,
  Row,
  Col
} from 'reactstrap';
import Link from 'next/link'
import Router from 'next/router'



class NavHeader extends Component {

  constructor(props){
    super(props);
    this.state = {
      dropdownOpen: false
    }
  }

  goToDemo = () => {
    Router.push("/#dream");
    let newURL = location.href.split("/")[0];
    window.history.pushState('object', document.title, newURL);
  }
  goToFeatures = () => {
    Router.push("/#features");
    let newURL = location.href.split("/")[0];
    window.history.pushState('object', document.title, newURL);
  }
  goToWhyChoose = () => {
    Router.push("/#whoAreWe");
    let newURL = location.href.split("/")[0];
    window.history.pushState('object', document.title, newURL);
  }
  goToServices = () => {
    Router.push("/#services");
    let newURL = location.href.split("/")[0];
    window.history.pushState('object', document.title, newURL);
  }
  goToContacts = () => {
    Router.push("/#contactSupport");
    let newURL = location.href.split("/")[0];
    window.history.pushState('object', document.title, newURL);
  }


  openDropdown = async () => {
    await this.setState({
      dropdownOpen: !this.state.dropdownOpen
    });
  }

  render(){
    const list = [
      {clickFunc: this.goToDemo, name: 'Demos'},
      {clickFunc: this.goToFeatures, name: 'Features'},
      {clickFunc: this.goToWhyChoose, name: 'About'},
      {clickFunc: this.goToServices, name: 'Services'},
      {clickFunc: this.goToContacts, name: 'Support'},
    ]
    return (
      <div className="nav">
        <Container>
          <div className="navContent">
            <div className="navItems">
              <div className="leftItems" onClick={() => Router.push('/')}>
                <img src="../static/images/logoSymbol.svg" alt=""/>
                <span>DREAM ICO</span>
              </div>
              <ul>
                {
                  list.map(( item, index ) => (
                    <li key={index} className="links">
                      <a onClick={item.clickFunc}>{item.name}</a>
                    </li>
                  ))
                }
                <li className="moreDropdown">
                  <img  className="moreDropdownImg"
                        src="../static/images/more.svg"
                        onClick={this.openDropdown}
                        alt=""
                  />
                </li>
              </ul>
            </div>
          </div>
        </Container>
        <ul className={this.state.dropdownOpen === true ? 'dropdownActive' : 'dropdownInactive'}
        >
          <Container>
            {
              list.map(( item, index ) => (
                <li key={index} className="mobileLinks">
                  <a onClick={item.clickFunc}>{item.name}</a>
                </li>
              ))
            }
          </Container>
        </ul>
       <style jsx>
         {`
           @import url('https://fonts.googleapis.com/css?family=Audiowide');
           @import url('https://fonts.googleapis.com/css?family=Montserrat:700');
           .nav {
             width: 100%;
             margin: 0;
             background-color: #1F231F;

             z-index: 100;
           }
           :global(.nav ul.dropdownActive) {
             display: block;
             width: 100%;
             height: 250px;
             background-color: #1F231F;
             position: absolute;
             opacity: 0.9;
             z-index: 100;
             padding: 20px 10px;
             margin: 0;
           }
           :global(.nav ul.dropdownInactive) {
             display: none;

           }
           :global(.nav ul li.mobileLinks) {
             margin: 15px 0;
             list-style: none;
           }
           :global(.nav ul li.mobileLinks a) {
             color: #fff;
             font-family: 'Montserrat';
             font-size: 14px;
           }
           .nav .navContent {
             margin: 0 auto;
             padding: 30px 0;
           }
           .nav .navContent .navItems .leftItems {
             display: flex;
             justify-content: flex-start;
             align-items: center;
             float: left;
             color: #fff;
             position: absolute;
             top: 10px;
             cursor: pointer;
           }
           .nav .navContent .navItems .leftItems span {
             font-family: 'Audiowide';
             margin-left: 10px;
             position: relative;
             top: 3px;
             font-size: 20px;
           }
           .nav .navContent .navItems ul {
             display: flex;
             justify-content: flex-end;
             float: right;
             list-style: none;
             position: relative;
             top: -6px;
           }
           .nav .navContent .navItems ul li{
             margin-right: 15px;
             margin-left: 15px;
           }
           :global(.nav .navContent .navItems ul li.moreDropdown) {
             display: none;
           }
           :global(.nav .navContent .navItems ul li img.moreDropdownImg) {
             width: 20px;
             cursor: pointer;
           }
           @media(max-width: 840px){
             :global(.nav .navContent .navItems ul li.moreDropdown) {
               display: block;
             }
             :global(.nav .navContent .navItems ul li.links) {
               display: none;
             }
           }
           .nav .navContent .navItems ul li a{
             color: #fff;
             font-family: 'Montserrat';
             font-weight: bold;
             text-decoration: none;
             transition: 0.3s;
           }
           .nav .navContent .navItems ul li a:hover{
             opacity: 0.6;
             transition: 0.3s;
           }
         `}
       </style>
      </div>
    );
  }
}
export default NavHeader;

import React, {Component} from 'react';
import { Container } from 'reactstrap';
import Particles from 'react-particles-js';
import ParticleEffectButton from 'react-particle-effect-button'
import $ from 'jquery';
import Link from 'next/link';
import Router from 'next/router';




class LandingShowcase extends Component {

  constructor(props){
    super(props);
    this.state = {
      hidden: false
    }
  }

  componentDidMount() {
    window.onscroll = () => {
      if(window.pageYOffset === 0) {
        this.setState({
          hidden: false
        })
      }
    };
  }

  click = () => {
    this.setState({
      hidden: true
    })
    setTimeout(function(){
      Router.push("/#dream");
      let newURL = location.href.split("/")[0];
      window.history.pushState('object', document.title, newURL);
    }, 1300)
  }

  render(){
    return(
      <div className="landingShowcase">
      <Particles style={{position: "absolute"}} className="canvas"/>
        <Container>
          <div className="landingShowcaseContent">
            <h1>Cryptocurrency and Blockchain Outsourcing</h1>
            <p>Focus on your business, we solve your problems</p>
            <div className="showcaseBottom">
              <p>Dream ICO Samples</p>
              <div className="bottomArrow">
                <ParticleEffectButton color='#FFA751' hidden={this.state.hidden}>
                  <img src="../../static/images/downArrow-Banner.svg" onClick={this.click} alt=""/>
                </ParticleEffectButton>
              </div>
            </div>
          </div>
        </Container>
        <style jsx>
          {`
            @import url('https://fonts.googleapis.com/css?family=Montserrat:300,700,800');
            .landingShowcase {
              width: 100%;
              height: 100%;
              overflow: hidden;
              background: url('../../static/images/landingpageBanner.png');
              background-repeat: no-repeat;
              background-position: center center;
              background-attachment: fixed;
              background-size: cover;
              margin: 0;
            }
            .landingShowcase .landingShowcaseContent {
              margin: 0 auto;
              position: relative;
            }
            .landingShowcase .landingShowcaseContent .canvas {
              width: 100% !important;
              height: 100% !important;
            }
            .landingShowcase .landingShowcaseContent h1 {
              font-family: 'Montserrat';
              text-align: center;
              font-weight: bold;
              width: 100%;
              margin: 0 auto;
              color: #fff;
              display: flex;
              justify-content: center;
              padding-top: 200px;
              font-size: 62px;
            }
            .landingShowcase .landingShowcaseContent p {
              font-family: 'Montserrat';
              width: 100%;
              margin: 20px auto;
              text-align: center;
              color: #e4e4e4;
              font-size: 26px;
            }
            .landingShowcase .landingShowcaseContent .showcaseBottom {
              position: relative;
              padding-top: 300px;
              padding-bottom: 100px;
            }
            .landingShowcase .landingShowcaseContent .bottomArrow {
              display: flex;
              justify-content: center;
              align-items: center;
            }
            .landingShowcase .landingShowcaseContent .bottomArrow img {
              cursor: pointer;
              z-index: 10;
            }
            .landingShowcase .landingShowcaseContent .showcaseBottom p{
              font-weight: 700;
              font-size: 20px;
              color: #FFA751;
            }

            @media(max-width: 1124px){
              .landingShowcase .landingShowcaseContent h1 {
                font-family: 'Montserrat';
                text-align: center;
                font-weight: bold;
                margin: 0 auto;
                color: #fff;
                display: flex;
                justify-content: center;
                padding-top: 200px;
                font-size: 50px;
              }
              .landingShowcase .landingShowcaseContent p {
                font-family: 'Montserrat';
                width: 70%;
                margin: 20px auto;
                text-align: center;
                color: #e4e4e4;
                font-size: 20px;
              }
              .landingShowcase .landingShowcaseContent .showcaseBottom {
                padding-top: 350px;
              }
              .landingShowcase .landingShowcaseContent .bottomArrow {
                display: flex;
                justify-content: center;
                align-items: center;
              }
              .landingShowcase .landingShowcaseContent .showcaseBottom p{
                font-weight: 700;
                font-size: 20px;
                color: #FFA751;
              }
            }
            @media(max-width: 824px){
              .landingShowcase .landingShowcaseContent h1 {
                font-size: 40px;
              }
            }
          `}
        </style>
      </div>
    )
  }
}
export default LandingShowcase;

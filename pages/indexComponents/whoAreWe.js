import React, {Component} from 'react';
import { Container, Row, Col } from 'reactstrap';

const WhoAreWe = () => (
  <div className="whoAreWe" id="whoAreWe">
    <Container>
      <Row className="whoAreWeContent">
        <Col md="6">
          <h2>About Us</h2>
          <p>We provide ICO, Cryptocurrency, and Blockchain outsourcing Services and ready-made products for anybody who wants to venture the booming cryptocurrency industry.</p>
        </Col>
        <Col md="6">
          <img src="../../static/images/AboutUsSampleImage.svg" alt=""/>
        </Col>
      </Row>
    </Container>

    <style jsx>
      {`
        @import url('https://fonts.googleapis.com/css?family=Montserrat:700,300');
        .whoAreWe {
          width: 100%;
          height: 100%;
          color: #fff;
          margin: 0;
          background-image: radial-gradient(79% 243%, #FFC355 72%, #FFA751 100%);
        }
        :global(.whoAreWe .whoAreWeContent) {
          width: 100%;
          margin: 0 auto;
          padding: 70px 0;
          display: flex;
          align-items: center;
          flex-flow: row wrap;
        }
        :global(.whoAreWe .whoAreWeContent h2) {
          font-family: 'Montserrat';
          margin: 0;
          color: #fff;
        }
        :global(.whoAreWe .whoAreWeContent p) {
          font-family: 'Montserrat';
          margin: 30px auto;
          font-size: 18px;
          text-align: justify;
        }
        :global(.whoAreWe .whoAreWeContent .col-md-6){
          text-align: center;
        }
        :global(.whoAreWe .whoAreWeContent img) {
          width: 100%;
        }
      `}
    </style>
  </div>
)
export default WhoAreWe;

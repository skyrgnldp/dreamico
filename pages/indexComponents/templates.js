import React, {Component} from 'react';
import { Container, Row, Col } from 'reactstrap';



const demos = [
  {
    img: "../../static/images/vpncashTemplate.png",
    title: "VPN Cash",
    body: "Cryptocurrency Platform Template 1",
    button: "Visit",
    href: "https://vpncash.org/"
  },
  {
    img: "../../static/images/dtcTemplate.png",
    title: "Dream Tech",
    body: "Cryptocurrency Platform Template 2",
    button: "Visit",
    href: "https://bitdtc.com/"
  },
  {
    img: "../../static/images/rainbowTemp.png",
    title: "Rainbow",
    body: "Cryptocurrency Platform Template 3",
    button: "Visit",
    href: "http://prd.rainbow88.org/"
  },
  {
    img: "../../static/images/linejrTemp.png",
    title: "LineJr",
    body: "Cryptocurrency Platform Template 4",
    button: "Visit",
    href: "https://linejr.com/"
  }
]

const Templates = () => (
  <div className="templates" id="dream">
    <Container>
      <div className="templatesContent">
        <div className="titlebar">
          <h3>Dream ICO Sample Demos</h3>
          <p>
            Our cryptocurrency and ICO platforms support all the widely used cryptocurrencies
          </p>
        </div>
        <div className="templateBlocks">
          {
            demos.map((demo, index ) => {
              return(
                <div className="view view-first" key={index}>
                  <img src={demo.img} alt=""/>
                  <div className="mask">
                    <h2>{demo.title}</h2>
                    <p>{demo.body}</p>
                    <a href={demo.href} target="_blank" className="info">{demo.button}</a>
                  </div>
                </div>
              )
            })
          }
        </div>
      </div>
    </Container>

    <style jsx>
      {`
        @import url('https://fonts.googleapis.com/css?family=Montserrat:300,700');
        .templates {
          width: 100%;
          height: 100%;
          overflow: hidden;
          background-color: #e4e4e4;
          margin: 0;
        }
        .templates .templatesContent {
          margin: 0 auto;
          padding: 50px 0;
        }
        .templates .templatesContent .titlebar h3 {
          font-family: 'Montserrat';
          text-align: center;
          font-weight: bold;
          margin: 0;
        }
        .templates .templatesContent .titlebar p {
          font-family: 'Montserrat';
          text-align: center;
          font-size: 30px;
        }
        .templates .templatesContent .titlebar p:nth-child(2) {
          width: 100%;
          margin: 20px auto;
          font-size: 18px;
        }
        :global(.templates .templatesContent .templateBlocks) {
          font-family: 'Montserrat';
          font-weight: bold;
          width: 100%;
          margin: 0 auto;
          display: flex;
          justify-content: center;
          align-items: center;
          padding: 0;
        }
        @media(max-width: 709px){
          :global(.templates .templatesContent .templateBlocks){
            width: 300px;
            flex-flow: row wrap;
          }
        }

        :global(.templates .templatesContent .templateBlocks .view) {
          margin: 10px;
          float: left;
          width: 100%;
          border: 10px solid #fff;
          overflow: hidden;
          position: relative;
          text-align: center;
          box-shadow: 1px 1px 2px #e6e6e6;
          cursor: default;

        }
        :global(.templates .templatesContent .templateBlocks .view .mask, .view .content) {
          width: 100%;
          height: 100%;
          position: absolute;
          overflow: hidden;
          top: 0;
          left: 0
        }
        :global(.templates .templatesContent .templateBlocks .view img) {
          display: block;
          position: relative;
        }

        :global(.templates .templatesContent .templateBlocks .view h2) {
          font-family: 'Montserrat' !important;
          text-transform: uppercase;
          color: #fff;
          text-align: center;
          position: relative;
          font-size: 17px;
          font-family: Raleway, serif;
          padding: 10px;
          margin: 20px 0 0 0
        }

        :global(.templates .templatesContent .templateBlocks .view p) {
          font-family: 'Montserrat' !important;
          font-weight: 300;
          font-size: 14px;
          position: relative;
          color: #fff;
          padding: 0px 20px 0px;
          text-align: center
        }

        :global(.templates .templatesContent .templateBlocks .view a.info) {
          font-family: 'Montserrat' !important;
          display: inline-block;
          text-decoration: none;
          padding: 7px 14px;
          background: #3c6382;
          color: #fff;
          text-transform: uppercase;
          box-shadow: 0 0 1px #3c6382
        }
        :global(.templates .templatesContent .templateBlocks .view a.info:hover) {
          box-shadow: 0 0 5px #000
        }

        :global(.templates .templatesContent .templateBlocks .view-first img) {
          /*1*/
          transition: all 0.2s linear;
          width: 100%;
        }

        :global(.templates .templatesContent .templateBlocks .view-first .mask) {
          opacity: 0;
          background-color: rgba(35, 35, 35, 0.80);
          transition: all 0.4s ease-in-out;
        }

        :global(.templates .templatesContent .templateBlocks .view-first h2) {
          transform: translateY(-100px);
          opacity: 0;
          font-family: Raleway, serif;
          transition: all 0.2s ease-in-out;
        }

        :global(.templates .templatesContent .templateBlocks .view-first p) {
          transform: translateY(100px);
          opacity: 0;
          transition: all 0.2s linear;
        }

        :global(.templates .templatesContent .templateBlocks .view-first a.info) {
          opacity: 0;
          transition: all 0.2s ease-in-out;
        }


        /* */

        :global(.templates .templatesContent .templateBlocks .view-first:hover img) {
          transform: scale(1.1);
        }

        :global(.templates .templatesContent .templateBlocks .view-first:hover .mask) {
          opacity: 1;
        }

        :global(.templates .templatesContent .templateBlocks .view-first:hover h2,
        .templates .templatesContent .templateBlocks .view-first:hover p,
        .templates .templatesContent .templateBlocks .view-first:hover a.info) {
          opacity: 1;
          transform: translateY(0px);
        }

        :global(.templates .templatesContent .templateBlocks .view-first:hover p) {
          transition-delay: 0.1s;
        }

        :global(.templates .templatesContent .templateBlocks .view-first:hover a.info) {
          transition-delay: 0.2s;
        }

      `}
    </style>
  </div>
)
export default Templates;

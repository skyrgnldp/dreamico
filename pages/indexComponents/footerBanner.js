import React, {Component} from 'react';


const FooterBanner = () => (
  <div className="footerBanner">
    <div className="footerBannerContent">
      <div className="banner">
        <img style={{width: '100%'}} src="../../static/images/DreamICO-icon.svg" alt=""/>
        <div className="titlebar">
          <h3>DREAM ICO</h3>
          <p>Cryptocurrency Business Solutions</p>
        </div>
      </div>
      <div className="bottom">
        <p>Copyright © Dream-ICO 2018. All rights reserved.</p>
      </div>
    </div>

    <style jsx>
      {`
        @import url('https://fonts.googleapis.com/css?family=Montserrat:300');
        @import url('https://fonts.googleapis.com/css?family=Audiowide');
        .footerBanner {
          width: 100%;
          height: 100%;
          overflow: hidden;
          margin: 0;
        }
        .footerBanner .footerBannerContent {
          width: 100%;
          margin: 0 auto;
        }
        .footerBanner .footerBannerContent .banner {
          width: 100%;
          margin: 0 auto;
          padding: 140px 0 100px 0;
          background: url('../../static/images/FooterBanner-BG.svg');
          background-repeat: no-repeat;
          background-position: center center;
          background-attachment: fixed;
          background-size: cover;
          color: #fff;
        }
        .footerBanner .footerBannerContent .banner img{
          display: block;
          margin-left: auto;
          margin-right: auto;
          width: 210px;
          height: 210px;
        }
        .footerBanner .footerBannerContent .banner .titlebar{

        }
        .footerBanner .footerBannerContent .banner .titlebar h3{
          font-family: 'Audiowide';
          color: #fff;
          margin: 20px 0;
          font-size: 50px;
          text-align: center;
        }
        .footerBanner .footerBannerContent .banner .titlebar p{
          font-family: 'Montserrat';
          font-size: 30px;
          text-align: center;
          color: #fff;
          font-weight: 300;
        }
        .footerBanner .footerBannerContent .bottom {
          font-family: 'Montserrat';
          width: 100%;
          height: 95px;
          background-color: #1F231F;
          display: flex;
          justify-content: center;
          align-items: center;
          color: #fff;
          font-size: 12px;
        }

      `}
    </style>
  </div>
)
export default FooterBanner;

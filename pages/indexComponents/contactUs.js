import React, {Component} from 'react';
import { Container, Row, Col } from 'reactstrap';
import { message } from 'antd';



class ContactUs extends Component {

  constructor(props){
    super(props);
    this.state = {

    }
  }


  validateFields = async e => {
    const {
      email,
      name,
      feedbackMessage
    } = this.state;

    if(!email && !name && !message){
      message.error('All input fields are required!');
    }else if(!email){
      message.error('Email should not be blank!');
    }else if(!name){
      message.error('You can put an optional name');
    }else if(!feedbackMessage){
      message.error('Please state your feedback message!');
    }else {
      await this.submitMessage();
    }
  }
  submitMessage = async (e) => {
    // await fetch('http://localhost:8080/api/contact/feedbacks', {
    //   method: 'POST',
    //   headers: {
    //     'Content-Type': 'application/json'
    //   },
    //   body: JSON.stringify({
    //     email: this.state.email,
    //     name: this.state.name,
    //     message: this.state.message
    //   })
    // })
      message.success("Thank you for your feedback!");
      this.clearInputs();
  }

  onChange = async e => {
    await this.setState({
      [e.target.name]: e.target.value
    });
  }

  clearInputs = () => {
    this.refs.email.value = '';
    this.refs.name.value = '';
    this.refs.message.value = '';
    this.setState({
      name: '',
      email: '',
      message: ''
    });
  }


  render(){
    return(
      <div className="contactUs" id="contactSupport">
        <Container>
          <div className="contactUsContent">
            <div className="titlebar">
              <span>Contact Us</span>
            </div>
            <Row>
              <Col md="6" className="contacts">
                <p>Any questions? React out to us and we'll get <br/>back to your shortly.</p>
                <img src="../../static/images/icon-location.svg" alt=""/>
                <span>Address: PBCOM Tower Ayala Avenue, Makati City</span><br/>
                <img src="../../static/images/phone-ContactUs-01.svg" alt=""/>
                <span>+63 945 210 6218</span><br/>
                <img src="../../static/images/mail-ContactUs-01.svg" alt=""/>
                <span>sky.alyssagarcia@gmail.com</span><br/>
                {/* <img src="../../static/images/telegram-ContactUs-01.svg" alt=""/>
                <span ><a href="https://t.me/bitdtc" target="_blank">Join us on Telegram</a></span> */}
              </Col>
              <Col md="6" className="inputs">
                <input type="text"
                       placeholder="Your Name"
                       ref="name"
                       name="name"
                       onChange={this.onChange}
                />
                <br/>
                <input type="email"
                       placeholder="Your Email"
                       ref="email"
                       name="email"
                       onChange={this.onChange}
                />
                <br/>
                <textarea placeholder="Your Message"
                          cols="30"
                          rows="5"
                          ref="message"
                          name="feedbackMessage"
                          onChange={this.onChange}
                />
                <br/>
                <button onClick={this.validateFields}>SUBMIT</button>
              </Col>
            </Row>
          </div>
        </Container>

        <style jsx>
          {`
            @import url('https://fonts.googleapis.com/css?family=Montserrat:500');
            .contactUs {
              width: 100%;
              height: 100%;
              margin: 0;
              background-color: #F5F5F5;
              overflow: hidden;
            }
            .contactUs .contactUsContent{
              margin: 0 auto;
              padding: 80px 0;
            }
            .contactUs .contactUsContent .titlebar{
              text-align: justify;
              -moz-text-align-last: center;
              text-align-last: center;
            }
            .contactUs .contactUsContent .titlebar span{
              font-size: 30px;
              color: #1F231F;
            }
            .contactUs .contactUsContent .titlebar p{
              width: 75%;
              margin: 30px auto;
              font-size: 18px;
            }
            :global(.contactUs .contactUsContent) {
              margin-top: 80px;
            }
            :global(.contactUs .contactUsContent .contacts) {
              margin-top: 50px;
              font-size: 14px;
            }
            :global(.contactUs .contactUsContent .contacts p){
              font-family: 'Montserrat';
              font-weight: bold;
            }
            :global(.contactUs .contactUsContent .contacts span){
              font-family: 'Montserrat';
              margin-left: 30px;
            }
            :global(.contactUs .contactUsContent .contacts span a){
              color: #1F231F;
            }
            :global(.contactUs .contactUsContent .contacts img){
              margin: 10px 0;
            }
            :global(.contactUs .contactUsContent .inputs) {
              font-family: 'Montserrat';
              margin-top: 40px;
              padding: 0 30px;
            }
            :global(.contactUs .contactUsContent .inputs input){
              border: none;
              color: #FFA751;
              width: 100%;
              font-size: 14px;
              height: 40px;
              padding-bottom: 20px;
              margin: 12px 0;
              background-color: transparent;
              border-bottom: 2px solid #FFA751;
            }
            :global(.contactUs .contactUsContent .inputs input:focus){
              outline: none;
            }
            :global(.contactUs .contactUsContent .inputs input::placeholder){
              color: #FFA751;
            }
            :global(.contactUs .contactUsContent .inputs textarea){
              border: none;
              padding-bottom: 20px;
              font-size: 14px;
              color: #FFA751;
              width: 100%;
              resize: none;
              margin: 12px 0;
              background-color: #f5f5f5;
              border-bottom: 2px solid #FFA751;
            }
            :global(.contactUs .contactUsContent .inputs textarea::placeholder){
              color: #FFA751;
            }
            :global(.contactUs .contactUsContent .inputs textarea:focus){
              outline: none;
            }
            :global(.contactUs .contactUsContent .inputs button){
              background-image: linear-gradient(-180deg, #FFE259 0%, #FFA751 100%);
              border:none;
              width: 125px;
              height: 50px;
              color: #fff;
              font-weight: bold;
            }
            @media(max-width: 991px){
              :global(.contactUs .contactUsContent .inputs button){
                width: 100%;
              }
            }
            :global(.contactUs .contactUsContent .inputs button:focus){
              outline: none;
            }
          `}
        </style>
      </div>
    );
  }
}
export default ContactUs;

import React, {Component} from 'react';
import { Container } from 'reactstrap';



const items = [
  {img:"../../static/images/pic-3.jpg",title: "Crypto Websites", body: "We make cryptocurrency websites that anybody can use!"},
  {img:"../../static/images/pic-1.jpg",title: "Smart Contracts", body: "We offer smart contract services customized to your needs"},
  {img:"../../static/images/pic-2.jpg",title: "Security", body: "We value data privacy. We provide end-to-end encryption to secure all user's personal data"},
  {img:"../../static/images/pic-4.jpg",title: "Verification", body: "Our verification process involves KYC to protect all parties involved in the platform"},
  {img:"../../static/images/pic.jpg",title: "Exchange", body: "We provide fast and reliable service of processing payments and buying of tokens"},
  {img:"../../static/images/2-layers-1.jpg",title: "Support Service", body: "We always respond to your messages. Feel free to contact us for support, suggestions and/or partnerships"},
]


const Features = () => (
  <div className="features" id="features">
    <Container>
      <div className="featuresContent">
        <div className="titlebar">
          {/* FEATURES */}
          <h3>What We Offer</h3>
        </div>
        <div className="featureBlocks">
          <div className="displayflexcenter">
            {
              items.map((item, index) => {
                return (
                  <div className="borderWhite" key={index}>
                    <img src={item.img} alt=""/>
                    <h5>{item.title}</h5>
                    <p>{item.body}</p>
                  </div>
                )
              })
            }
          </div>
        </div>
      </div>
    </Container>

    <style jsx>
      {`
        @import url('https://fonts.googleapis.com/css?family=Montserrat:300,700');
        .features {
          width: 100%;
          height: 100%;
          margin: 0;
          color: #fff;
          background: url('../../static/images/featuredBackground.png');
          background-repeat: no-repeat;
          background-position: center center;
          background-attachment: fixed;
          background-size: cover;
        }
        .features .featuresContent {
          width: 100%;
          margin: 0 auto;
          padding: 50px 0;
        }
        .featuresContent .titlebar h3 {
          font-family: 'Montserrat';
          text-align: center;
          color: #fff;
          margin: 0;
        }
        .featuresContent .titlebar p {
          font-family: 'Montserrat';
          text-align: center;
          font-size: 18px;
          width: 75%;
          margin: 30px auto;
        }
        .featureBlocks {
          width: 100%;
          margin: 0 auto;
          padding-bottom: 50px;
        }
        .featureBlocks .borderWhite {
          width: 280px;
          height: 280px;
          border: 1px solid #fff;
          margin: 40px 30px;
        }
        .featureBlocks .borderWhite img{
          margin: 10px auto;
          width: 88%;

        }
        .featureBlocks .borderWhite h5{
          font-size: 16px;
          font-weight: bold;
          color: #fff;
        }
        .featureBlocks .borderWhite p{
          margin: 10px auto;
          width: 88%;
          font-size: 12px;
          text-align: justify;
          line-height: 15px;
        }
        .featureBlocks .displayflexcenter {
          font-family: 'Montserrat';
          display: flex;
          justify-content: center;
          align-items: center;
          text-align: center;
          flex-flow: row wrap;
        }
      `}
    </style>
  </div>
)
export default Features;

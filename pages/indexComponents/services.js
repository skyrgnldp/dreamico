import React, {Component} from 'react';
import { Container } from 'reactstrap';
import {Icon, Tooltip} from 'antd';



const cards = [
  {
    img: "../../static/images/ServicesSampleImage.svg",
    title: "Lorem ipsum",
    body: "Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.",
    price: "$10"
  },
  {
    img: "../../static/images/ServicesSampleImage2.svg",
    title: "Lorem ipsum",
    body: "Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.",
    price: "$10"
  },
  {
    img: "../../static/images/ServicesSampleImage3.svg",
    title: "Lorem ipsum",
    body: "Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.",
    price: "$10"
  },
  {
    img: "../../static/images/ServicesSampleImage4.svg",
    title: "Lorem ipsum",
    body: "Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.",
    price: "$10"
  },
  {
    img: "../../static/images/ServicesSampleImage5.svg",
    title: "Lorem ipsum",
    body: "Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.",
    price: "$10"
  },
  {
    img: "../../static/images/ServicesSampleImage6.svg",
    title: "Lorem ipsum",
    body: "Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.",
    price: "$10"
  }
]

const Services = () => (
  <div className="services" id="services">
    <Container>
      <div className="servicesContent">
        <div className="titlebar">
          <h3>Dream ICO Services</h3>
          <p>
            We offer cryptocurrency and blockchain related widgets and services that you can include in your websites
          </p>
        </div>
        <div className="servicesBlocks">
          <div className="displayflexcenter">

            {
              cards.map((item, index) => {
                return (
                  // <div className="serviceFrame" key={index}>
                  <div key={index}>
                    <img src={item.img} alt=""/>
                    {/* <h5></h5>
                    <p></p> */}
                  </div>
                )
              })
            }
          </div>
        </div>
      </div>
    </Container>

    <style jsx>
      {`
        @import url('https://fonts.googleapis.com/css?family=Montserrat:300,700');
        .services {
          width: 100%;
          height: 100%;
          margin: 0;
          font-family: 'Montserrat';
        }
        .services .servicesContent {
          margin: 0 auto;
          padding: 70px 0;
        }
        .services .servicesContent .titlebar {
          text-align: center;
        }
        .services .servicesContent .titlebar h3{
          font-weight: bold;
        }
        .services .servicesContent .titlebar p{
          width: 100%;
          font-size: 15px;
          margin: 30px auto;
        }
        .services .servicesContent.servicesBlocks {
          width: 100%;
          margin: 0 auto;
        }
        .services .servicesBlocks .displayflexcenter {
          display: flex;
          justify-content: center;
          align-items: center;
          flex-flow: row wrap;
        }
        .services .servicesBlocks .displayflexcenter img {
          margin: 10px;
          width: 95%;
          -webkit-box-shadow: 10px 10px 24px -6px rgba(138,128,138,1);
          -moz-box-shadow: 10px 10px 24px -6px rgba(138,128,138,1);
          box-shadow: 10px 10px 24px -6px rgba(138,128,138,1);
        }

      `}
    </style>
  </div>
)
export default Services;

import React from 'react'
import Link from 'next/link'
import {message, BackTop} from 'antd';
import $ from 'jquery';
import Head from '../components/head'
import Nav from '../components/nav'
import LandingShowcase from './indexComponents/landingShowcase';
import Templates from './indexComponents/templates';
import Features from './indexComponents/features';
import WhoAreWe from './indexComponents/whoAreWe';
import Services from './indexComponents/services';
import ContactUs from './indexComponents/contactUs';
import FooterBanner from './indexComponents/footerBanner';

message.config({
  top: 50,
  duration: 2,
  maxCount: 1,
});



export default class Home extends React.Component {

  render(){
    return(
      <div>
          <Head />
          <Nav />
          <LandingShowcase />
          <Templates />
          <Features />
          <WhoAreWe />
          <Services />
          <ContactUs />
          <FooterBanner />

          <BackTop />

        <style jsx global>
          {`
            html {
              scroll-behavior: smooth;
            }
            #goToTopButton {
              display: none;
              position: fixed;
              bottom: 20px;
              right: 30px;
              z-index: 99;
              font-size: 18px;
              border: none;
              outline: none;
              background-color: #95afc0;
              color: white;
              cursor: pointer;
              padding: 15px;
              border-radius: 100%;
            }
            .goToTopButton:hover {
              background-color: #535c68;
            }
          `}
        </style>
      </div>
    );
  }
}
